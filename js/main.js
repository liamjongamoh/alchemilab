$(document).ready(function () {
    $(function () {
        $('#menu li a').bind('click', function (event) {
            var $anchor = $(this);
            $anchor.parent().addClass('active').siblings().removeClass('active');
            
            var w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
            var offset = $anchor.parent().data('index');
            console.log(w*offset)
            $('#container').stop().animate({
                scrollTop: w*offset
            }, 1000);
            event.preventDefault();
        });
    });
});